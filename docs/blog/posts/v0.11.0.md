---
date:
  created: 2022-11-27
authors:
  - moul
categories:
  - release
---

# Silkaj v0.11.0 release

Silkaj team is pleased to announce the stable release of Silkaj version 0.11.0

The most important changes are highlighted in this article, if you are looking for a comprehensive list of changes, check out the changelog.

## Introduction

This release comes with the `pipx` installation method, a new design of command line interface with commands separated in groups.
Last but not least, it contains the migration to `Texttable` table library, network improvements and fixes, and Python v3.11 support.

On the development environment side, the repository has been restructured, the code has been annotated with types, and linted.
`mypy` and `pylint` pre-commit hooks have been introduced. The tests have been separated into unit and integration tests.
`main` branch is new name of the main branch.

This release emphasis was put onto strengthen the development environment as well as improving the structure of Silkaj code base.
The goal was to do preparations and cleaning before Substrate migration in order to be fully focus onto it in next development cycle.

This release is the latest planned major release with Duniter v1/BMA support and the one planned to end into Debian 12 Bookworm.

<!-- more -->

## `pipx` installation method

`pipx` have been found to be a recommendable tool over `pip` to install an end-user Python software when the latter is not available in distributions repositories.
In addition to `pip` features, `pipx` creates virtual environments, which prevents conflicting dependencies among several installed Python software.
A nice feature of `pipx` is the `ensurepath` commands which takes care to add the Python executables path into the `PATH` environment variable, which can be found to be the trickiest part of the installation.
Silkaj documentation has been updated to suggest `pipx` instead of `pip`.

In short, you can install Silkaj via `pipx` this way:

```
sudo apt install pipx libsodium23
pipx install silkaj
```

For a comprehensive documentation, check the [installation documentation](../../install.md).

## New CLI design

The commands have been grouped into following groups: `blockchain`, `money`, and `wot`.
The other commands not related to these topics have been kept at the root of the CLI tree.

```{..yaml .no-copy}
silkaj --help
  about           Display program information
  authentication  Generate authentication file
  blockchain      Blockchain related commands
  checksum        Generate checksum out of a passed pubkey or an...
  license         Display Ğ1 monetary license
  money           Money management related commands
  wot             Web-of-Trust related commands
```

```{..yaml .no-copy}
silkaj blockchain --help
  blocks      Display blocks: default: 0 for current window size
  difficulty  Display the current Proof of Work difficulty level to...
  info        Display information about currency
  verify      Verify blocks’ signatures.
```

```{..yaml .no-copy}
silkaj money --help
  balance   Get wallet balance
  history   Display transaction history
  transfer  Transfer money
```

```{..yaml .no-copy}
silkaj wot --help
  certify     Send certification
  lookup      User identifier and public key lookup
  membership  Send and sign membership document: for first emission and...
  revocation  Create, save, verify or publish revocation document.
  status      Check received and sent certifications and consult the...
```

In this process following commands have been renamed:

```{..yaml .no-copy}
diffi −> difficulty
tx −> transfer
cert −> certify
revocation save −> revocation create
wot −> wot status
```

## Texttable

The code has been migrated from `tabulate` to `Texttable` external table libraries.
`Texttable` was introduced and was only used from the `history` command, where the other commands were using `tabulate`.
Both were used, and we had to make the choice to stick with one and get the opportunity to reduce the dependency list of one item.
The migration has been done. You will probably notice differences in the table style and the alignments in the columns.

Before, with `tabulate`:

```{..yaml .no-copy}
|     uid     |   blocks |   percent |
|-------------+----------+-----------|
|    vit2     |       33 |      33.0 |
|    Pini     |       25 |      25.0 |
|  moul-test  |       24 |      24.0 |
| jytou4-test |       10 |      10.0 |
| jytou5-test |        8 |       8.0 |
```

Now, with `texttable`:

```{..yaml .no-copy}
│─────────────│────────│─────────│
│     uid     │ blocks │ percent │
│═════════════│════════│═════════│
│ vit2        │     33 │      33 │
│ Pini        │     26 │      26 │
│ moul-test   │     23 │      23 │
│ jytou4-test │     10 │      10 │
│ jytou5-test │      8 │       8 │
│─────────────│────────│─────────│
```

## Network

Breaking network connection is something recurrent on local network with BMA.
A hack have been introduced to circumvent this issue with `sleep` calls between repetitive requests which retrieve user ids from public keys.

The singletons have been replaced with `@functools.lru_cache()` decorator usage.
They are both used to reduce the network traffic and therefore speed-up the commands execution.
They are basically allowing to reuse the result obtained from the first request in case the same request happen again.
In Silkaj, they are used to retrieve the UD value, the currency parameters, and the head block only once.
Singletons pattern has been a pain to handle specially within the tests. `@functools.lru_cache()` fixes that which comes in handy as a boilerplate.

## Argos

`argos` command have been removed. This command was displaying basic information similar to the `info` command but formatted for Argos.
Argos is a GNOME Shell extension which displays what has been generated out of a shell command into a GNOME Shell pop-over.
This command has been found not to be really used and poorly maintained. It has been decided to be remove in the meantime.

## Python 3.11

This release introduces support for Python v3.11 released on the 24th of October 2022.

## Repository restructuration

As well as the new CLI commands grouping, the runtime and the tests code have been classified into following directories: `blockchain`, `money`, and `wot`.
In addition to that, there is now a Python file per command and files for "helpers" and tools placed aside.

## Development Environment

New `pre-commit` hooks have been introduced: `mypy`, `pylint`, `autoflake`, `pre-commit-hooks`, and `mdformat`.
Type annotations has been introduced in the whole runtime code.
This greatly improves the code quality, specially now that the code is fully typed, and linted.
It will be of great help in future developments.

The tests have been separated into `unit` and `integration` tests directories. This separation makes things clearer.
The unit tests are pretty quick to be performed, where the integration tests takes more time since they require network requests.
On the CI side, now just one job runs the integration tests in the job generating the coverage.
Integration tests were known to fail because of the unstable nature of the network. Now, in case the integration tests fails, it is only necessary to restart one job.

The branch workflow changed to a unique `main` branch which takes over the `dev` and `master` branches.

The coverage integration into GitLab has been fixed.

## Thanks

@moul, @matograine

## Outlook

v0.20.0 is planned to be the next release. It will be fully dedicated to add support for Duniter v2 based on Substrate framework.
Duniter v1/BMA support will be dropped from Silkaj v0.2x branch, while v0.1x will still be maintained until we productively switch to Duniter v2.
But, no new features will be implemented on v0.1x branch.
This development cycle might take some time, so it is expected to get preview releases.

v0.20.0 development cycle is planned as follow.
The network layer needs to be adapted to use following APIs: Substrate RPC, and Hasura indexer’s using GraphQL.
Then, It is planned to migrate current features and commands.
Finally, new v2 related features will be implemented.

In this development cycle, parallel developments might occur, such as setting up the new website or the configuration support implementation in Silkaj.

We are looking forward for a better future with the migration of Duniter on Substrate framework.
