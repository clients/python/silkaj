---
date:
  created: 2021-04-17
authors:
  - moul
categories:
  - release
---

# Silkaj v0.9.0 release

## Introduction

The Silkaj team is pleased to announce the release of Silkaj 0.9.0.
The most important changes are highlighted in this article, if you are looking for a comprehensive list of changes, check out the changelog.

<!-- more -->

#### Transaction

Silkaj is now properly handling the transaction document size limit.
The 100 lines limit length of the transaction document in the compact format is now properly fulfilled by computing the length of the generated document.

An important bug has been fixed regarding intermediaries transactions:
When spending lots of sources (i.e. huge amounts from member wallets), many useless intermediaries transactions were sent and displayed. Intermediaries transactions are now correctly handled.

Unit tests have been written on the `tx` command, which comforts us into developing new features.

#### Refactored `id`/`lookup` command

The `id` command has been completely refactored. It now offers comprehensive results when looking for an identity by specifying a user identifier or a public key.
Now the non-member user identifier are displayed.
The command now uses same algorithm as `choose_identity()` which relies exclusively on `/wot/lookup` BMA's path.
The command has been renamed to `lookup` to represent more closely what it does. This command renaming will also allow to introduce the future `identity` command.

```{ ..yaml .no-copy }
silkaj lookup titi
Public keys or user id found matching 'titi':

→ 4qJZFRfArLaUMEXDhsd69unsKynEFNLFazAVij4HNsBa:F4z ↔ laetitia97421
→ 4LCdTC9QsmqbFSHAhyaqGdDCVPr8Ywu2DZ8hDCzkdx4n:8ta ↔ Amandinelaetitia
→ CehfxBHrowP6tXouR73GS52QhGAoNMtabooKbCvT7f3j:6cG ↔ Laetiti974
→ FtZdA1HzHcDG6utoEgg6R5jjXfEne2ftS2UzvXajKurp:6Sm ↔ Laetitia
→ EUn8uLJxgc3fVXJ1fBA74re4iD4Ws8Nk2xHytX1wLMfK:AQ2 ↔ LaetitiaCarivenc
→ HqHZt9J1U7MwC3RF1bSjPgxACsTypfNjUThYFMZZfK6X:Dk1 ↔ LaetitiaHOFF
→ 5Vcm1zkHKoAMo9yy7Lk2HDX2Yn54agavtEUdrcFNmqkP:8mU ↔ laetitiajanot
→ BfncGdTeq8qvhPZnBaM8T76SHM9xfx78ASRNZtGp64rZ:7tP ↔ titi
→ BWKuSHYojjwzAXZZxSFTf5XVWneUMZqoWGUJEfWaFRL9:21F ↔ titi43
→ 49nWdTQqDT8qpazzPeP6NH92NwppG7YEh6PFYC2VecNA:Ba6 ↔ titix
```

#### New `--full-pubkey` option on the `history` command

The `history` command adds a new option to display the whole public keys `--full-pubkey`:

```{ ..yaml .no-copy }
silkaj -gt history 5B8iMAzq1dNmFe3ZxFTBQkqhq4fsztg1gZvxHXCk1XYH --full-pubkey

Transactions history from: moul-test 5B8iMAzq1dNmFe3ZxFTBQkqhq4fsztg1gZvxHXCk1XYH:baK
Current balance: 9012407.83 ĞTest, 161.34 UD ĞTest on the 2021-04-09 15:16:42
+---------------------+------------------------------+---------------+-----------------+-----------------------------+
|        Date         |      Issuers/Recipients      | Amounts ĞTest | Amounts UDĞTest |           Comment           |
+=====================+==============================+===============+=================+=============================+
| 2021-03-29 20:42:33 |                              |               |                 | Change operation            |
+---------------------+------------------------------+---------------+-----------------+-----------------------------+
| 2021-03-22 20:51:05 | WULdRTxspGdJzrs4vpZsWLGWsu37 | -193320       | -3.460          |                             |
|                     | DjqoHyhGDFr5amh:45s          |               |                 |                             |
+---------------------+------------------------------+---------------+-----------------+-----------------------------+
| 2021-03-22 20:51:05 | WULdRTxspGdJzrs4vpZsWLGWsu37 | -338310       | -6.060          |                             |
|                     | DjqoHyhGDFr5amh:45s          |               |                 |                             |
+---------------------+------------------------------+---------------+-----------------+-----------------------------+
```

#### `balance` command is now displaying corresponding member identity user identifier

```{ ..yaml .no-copy }
silkaj -gt balance 5B8iMAzq1dNmFe3ZxFTBQkqhq4fsztg1gZvxHXCk1XYH
╒══════════════════════════════╤══════════════════════════════════════════════════╕
│ Balance of pubkey            │ 5B8iMAzq1dNmFe3ZxFTBQkqhq4fsztg1gZvxHXCk1XYH:baK │
├──────────────────────────────┼──────────────────────────────────────────────────┤
│ User identifier              │ moul-test                                        │
├──────────────────────────────┼──────────────────────────────────────────────────┤
│ Total amount (unit|relative) │ 8634537.83 ĞTest | 162.21 UD ĞTest               │
├──────────────────────────────┼──────────────────────────────────────────────────┤
│ Total relative to M/N        │ 1.05 x M/N                                       │
╘══════════════════════════════╧══════════════════════════════════════════════════╛
```

#### Display option on `cert`, `membership` commands

This general option allows to display the generated document aside of the confirmation prompt before sending the document.
It can be used for debugging, safety, or curiosity purposes.
This option has only been implemented on the `cert` and the `membership` commands for now.
The option should be available in next releases for the three others send-documents commands.

```{ ..yaml .no-copy }
silkaj --gtest --auth-file --display cert KapisTest
╒════════╤══════════════════════════════════════════════════╤════╤══════════════════════════════════════════════════╕
│ Cert   │ Issuer                                           │ –> │ Recipient: Published: #block-hash date           │
├────────┼──────────────────────────────────────────────────┼────┼──────────────────────────────────────────────────┤
│ ID     │ moul-test                                        │ –> │ KapisTest: #673782-00001519… 2020-12-11 11:37:42 │
├────────┼──────────────────────────────────────────────────┼────┼──────────────────────────────────────────────────┤
│ Pubkey │ 5B8iMAzq1dNmFe3ZxFTBQkqhq4fsztg1gZvxHXCk1XYH:baK │ –> │ HGuKgbo7s8wjKF8gQwpdPQGG8mLW9vNMq1ZFxMEZgD8c:GZp │
├────────┼──────────────────────────────────────────────────┼────┼──────────────────────────────────────────────────┤
│ Valid  │ 2021-04-09                                       │ —> │ 2021-09-02                                       │
╘════════╧══════════════════════════════════════════════════╧════╧══════════════════════════════════════════════════╛
Version: 10
Type: Certification
Currency: g1-test
Issuer: 5B8iMAzq1dNmFe3ZxFTBQkqhq4fsztg1gZvxHXCk1XYH
IdtyIssuer: HGuKgbo7s8wjKF8gQwpdPQGG8mLW9vNMq1ZFxMEZgD8c
IdtyUniqueID: KapisTest
IdtyTimestamp: 673782-00001519FCBA6DDEFEE65B31ECD145B065F4404F870AAB272419EFA62BF5A677
IdtySignature: 1KX/Iuv7FuXsxhTwYU/RIO9L0F3I+lGvBLYXRexIoCz35H+yb2Wf3nEXA2XXCPl5aVxcxvreOW2HF43r7ezcDA==
CertTimestamp: 735968-00003F6B07F64573F6FABC510C1CEABEE8358426176BE7F190827A856873C517

Do you confirm sending this certification? [y/N]: y
Certification successfully sent.
```

```{ ..yaml .no-copy }
silkaj --gtest --auth-file --display membership
╒════════════════════════════════════════════════════╤══════════════════════════════════════════════════╕
│ Expiration date of current membership              │ in 2 months                                      │
├────────────────────────────────────────────────────┼──────────────────────────────────────────────────┤
│ User Identifier (UID)                              │ moul-test                                        │
├────────────────────────────────────────────────────┼──────────────────────────────────────────────────┤
│ Public Key                                         │ 5B8iMAzq1dNmFe3ZxFTBQkqhq4fsztg1gZvxHXCk1XYH:baK │
├────────────────────────────────────────────────────┼──────────────────────────────────────────────────┤
│ Block Identity                                     │ 167750-0000A51FF952B76AAA594A46CA0C8156A56988…   │
├────────────────────────────────────────────────────┼──────────────────────────────────────────────────┤
│ Identity published                                 │ April 20, 2018                                   │
├────────────────────────────────────────────────────┼──────────────────────────────────────────────────┤
│ Expiration date of new membership                  │ in 2 months                                      │
├────────────────────────────────────────────────────┼──────────────────────────────────────────────────┤
│ Expiration date of new membership from the mempool │ in 2 weeks                                       │
╘════════════════════════════════════════════════════╧══════════════════════════════════════════════════╛
Version: 10
Type: Membership
Currency: g1-test
Issuer: 5B8iMAzq1dNmFe3ZxFTBQkqhq4fsztg1gZvxHXCk1XYH
Block: 735967-00028030F3E2AB38D7EACA997065F58E16F0497EE86BFA23B2DA222EEE80C23C
Membership: IN
UserID: moul-test
CertTS: 167750-0000A51FF952B76AAA594A46CA0C8156A56988D2B2B57BE18ECB4F3CFC25CEC2
phOKWXHxfPBgkTE+Mui8Fiqb7QSlhPrKalDOzhyUwwLySV/EE2Z/b+vZNfByTn7sFYaHWPck5dsbzJQ6M+fBBA==

Do you confirm sending this membership document for this identity? [y/N]: y
Membership successfully sent
```

#### Dry Run option is becoming a generic option

Before:

```{ ..yaml .no-copy }
silkaj membership --dry-run
```

Now:

```{ ..yaml .no-copy }
silkaj --dry-run membership
```

Note: This option is currently only implemented in the `membership` command.

Note: The difference between the `--display` and the `--dry-run` options are that the dry run option by-passes the license, and the confirmation approvals.
For safety reasons, the `--dry-run` option doesn't allow, at the end, to send the document on the network.
On contrary, the `--display` option allows to send the document to the network.

#### Python support

Support for Python 3.5 has been dropped and support for Python 3.9 has been added.

#### Silkaj v0.8.1 in Debian Bullseye

If you are a user of Debian or its derivatives, you will be able to find Silkaj v0.8.1 available into Debian Bullseye (v11) which is about to be released.
Silkaj package has been updated from v0.6.5 to v0.8.1 and DuniterPy v0.60.1 entered Debian repository for the first time.

#### Outlook

In the next developments, we are planning to work on DeathReaper, the implementations of the `revoke` and the `identity` commands.
The removal of the asynchronous property, the migration from `tabulate` to `Texttable`, and the migration from BMA to GVA.

### Thanks

@matograine, @moul, @jonas, @atrax
