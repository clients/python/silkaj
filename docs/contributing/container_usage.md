There are two kind of containers. The one built with `pip` for user purposes, and the one built with Poetry for developer purposes.

We recommand using `podman` instead of `docker` command for not having to run the command as `root`.

In case you use `docker`, you can [add your user into the `docker` group, so you can manipulate `docker` executable without `root` permissions](https://askubuntu.com/a/739861).

#### User

Pull the image:

=== "Podman"

    ```
    podman pull registry.duniter.org/clients/python/silkaj/release/pip:latest
    ```

=== "Docker"

    ```
    sudo docker pull registry.duniter.org/clients/python/silkaj/release/pip:latest
    ```

Run Silkaj from outside the image:

=== "Podman"

    ```
    podman run -it registry.duniter.org/clients/python/silkaj/release/pip:latest silkaj info
    ```

=== "Docker"

    ```
    sudo docker run -it registry.duniter.org/clients/python/silkaj/release/pip:latest silkaj info
    ```

Go into the image:

=== "Podman"

    ```
    podman run -it registry.duniter.org/clients/python/silkaj/release/pip:latest bash
    ```

=== "Docker"

    ```
    sudo docker run -it registry.duniter.org/clients/python/silkaj/release/pip:latest bash
    ```

Then run Silkaj:

```
silkaj info
```

The working directory is where Silkaj sources are installed in Python `site-packages`.
This is fine for doing small editions. For bigger editions, it is adviced to use a development environment with Poetry.

#### Developer

`git` is installed, so it can be used as a development environment.

Pull the image:

=== "Podman"

    ```
    podman pull registry.duniter.org/clients/python/silkaj/release/poetry:latest
    ```

=== "Docker"

    ```
    sudo docker pull registry.duniter.org/clients/python/silkaj/release/poetry:latest
    ```

Run Silkaj from ouside the image:

=== "Podman"

    ```
    podman run -it registry.duniter.org/clients/python/silkaj/release/poetry:latest silkaj info
    ```

=== "Docker"

    ```
    sudo docker run -it registry.duniter.org/clients/python/silkaj/release/poetry:latest silkaj info
    ```

Go into the image:

=== "Podman"

    ```
    podman run -it registry.duniter.org/clients/python/silkaj/release/poetry:latest bash
    ```

=== "Docker"

    ```
    sudo docker run -it registry.duniter.org/clients/python/silkaj/release/poetry:latest bash
    ```

Then run Silkaj:

```
silkaj info
```

The working directory contains Silkaj sources. Set up the repository to have it ready for developments:

```bash
git checkout dev
git remote set-url origin https://git@git.duniter.org/clients/python/silkaj.git
```

```bash
git config --global user.email "you@example.com"
git config --global user.name "Your Name"
```
