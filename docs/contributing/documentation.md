# Documentation contribution

<center>
![MkDocs Material icon](../images/mkdocs-material-favicon.png)
</center>

The project documentation website is based on [MkDocs framework](https://www.mkdocs.org/) and on following theme and plugins:

- [Mkdocs Material](https://squidfunk.github.io/mkdocs-material/): theme and framework
- [Mike](https://github.com/jimporter/mike): to publish multiple versions
- [MkDocs Coverage](https://github.com/pawamoy/mkdocs-coverage): to integrate coverage report iframe into MkDocs
- [MkDocs GitLab plugin](https://pypi.org/project/mkdocs-gitlab-plugin/): to display links to GitLab references items: used in the changelog page

Please refer to their respective documentation.

## Set up

Install the optional `docs` group:

```
poetry install --with docs
```

## MkDocs

Run the website in development mode:

```
poetry run mkdocs deploy
```

Open [localhost:8000](http://localhost:8000/) in your web browser.

The preview will be live-updated once you change the documentation.

## Mike

Running following command will serve what’s on `pages` branch:

```
poetry run mike serve
```

This preview won’t be live-updated with your latest changes as it would be with `mkdocs serve`. This is because Mike simply serves what’s built on `pages` branch.

## Changing the content

Into `docs` folder, you can find the Markdown source files which can be edited.
`mkdocs.yml` file lists the website structure.

## Deployment workflow

Only on `main` branch, a `website` job will be triggered containing the documentation build for a specific version. It creates a commit which is added to `pages` branch with `poetry run mike deploy $version` command.
Then the commit is pushed on the git repository remote from the job with an access token.
The coverage report is integrated in the website build. The artefact is retrieved from the coverage job with `needs:` keyword.

From `pages` branch, an other pipeline is triggered, publishing the documentation website to [silkaj.duniter.org](https://silkaj.duniter.org) for all versions via GitLab Pages.

### Access token

A `git push` is performed from the job to the remote repository via the authorisation provided via an access token set in CI/CD variables.
The access token has to be renew every year, since this is the maximum lifespan we can set for an access token at its creation.
The access token has to be created on a project basis with `repository_write` checked and with Developer role.
