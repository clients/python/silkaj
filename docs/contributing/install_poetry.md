<center>
![Poetry logo](../images/poetry-logo.svg){: style="height:120px;width:120px"}
</center>

## Install Silkaj in a development environement with Poetry

### Install libsodium

=== "Debian"

    ```
    sudo apt install libsodium23
    ```

=== "Fedora"

    ```
    sudo dnf install libsodium
    ```

### Install Poetry

- [Installation documentation](https://python-poetry.org/docs/#installation)

```
pipx install poetry
```

### Install dependencies and the Python virtual environment

```bash
# Over HTTPS
git clone https://git.duniter.org/clients/python/silkaj.git

# Over SSH
git clone git@git.duniter.org:clients/python/silkaj.git

cd silkaj

# Installs the runtime and development dependencies
poetry install
```

### Run Silkaj

Within `silkaj` repository run Silkaj:

```bash
poetry run silkaj
```

### Poetry shell

You can access tools `pytest` from within the development environment with `poetry run` or by entering the shell:

```bash
silkaj> poetry run pytest
```

```bash
silkaj> poetry shell
(silkaj-58dUTebQ-py3.12) silkaj> pytest
```

### Make Silkaj accessible from everywhere

Add following alias to your shell configuration:

```bash
alias silkaj="cd /path/to/silkaj && poetry run silkaj"
```
