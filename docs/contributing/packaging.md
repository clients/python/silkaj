# Packaging

Silkaj is packaged in official Debian repositories. Packaging details can be found in [its repository on Debian forge](https://salsa.debian.org/cryptocoin-team/silkaj).

## Dependencies

DuniterPy might be the only dependency not already into distributions.

## Manual pages

To generate manual pages: #179

- [`click-man`](https://github.com/click-contrib/click-man#debian-packages)

## Shell completion

Shell completion might be set-up as described in the [installation documentation](../install.md/#shell-completion).
