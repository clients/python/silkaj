---
hide:
  - toc
  - navigation
---

<center>
# Welcome

![silkaj logo](images/silkaj_logo.png){ width="200" }

Welcome to Silkaj documentation

[Install :material-package-down:](install.md){ .md-button .md-button--primary } [Usage :material-keyboard-outline:](usage/index.md){ .md-button .md-button--primary }

## What is Silkaj?

Silkaj is a command line client for [Ğ1](https://duniter.org/g1/) libre currency powered by [Duniter software](https://duniter.org/).

<div class="grid cards" markdown>

- :fontawesome-solid-money-bill-1-wave:{ .lg .middle} __Money management__

    ______________________________________________________________________

    Transfer money, check your balance, and history stand.

- :material-vector-polyline:{ .lg .middle } __Web-of-Trust__

    ______________________________________________________________________

    Manage your certifications, membership, and the revocation document

- :octicons-terminal-16:{ .lg .middle } __CLI interface__

    ______________________________________________________________________

    Silkaj is built around a command line interface design.

- :material-book-open-variant:{ .lg .middle} __RTM compatible__

    ______________________________________________________________________

    Silkaj is compatible with the [Relative Theory of Money](https://en.trm.creationmonetaire.info/)
    and includes Ğ1 Monetary License.

- :material-scale-balance:{ .lg .middle } __Free software__

    ______________________________________________________________________

    Silkaj is licensed under :material-copyleft: :simple-gnu: [GNU AGPL v3](https://www.gnu.org/licenses/agpl-3.0.html).

- :material-raspberry-pi:{ .lg .middle } __Low requirements__

    ______________________________________________________________________

    Written with Python, Silkaj is built with low requirements in mind.

<!--
- Authentication method

    ______________________________________________________________________

    Wallet Import Format (WIF) is implemented.
-->

</div>
</center>
