---
hide:
  - navigation
---

# Install Silkaj

## Distribution

Installing Silkaj from the package manager of your favorite distribution might not be available or might not come with latest Silkaj version.
In order to get latest version install Silkaj with `pipx` or `pip` from PyPI which you can find in next sections.

[![Packaging status](https://repology.org/badge/vertical-allrepos/silkaj.svg?columns=2)](https://repology.org/project/silkaj/versions)

## Unix

On a Unix system, run in your shell following commands:

### Install libsodium

=== "Debian"

    ```
    sudo apt install libsodium23
    ```

=== "Fedora"

    ```
    sudo dnf install libsodium-devel
    ```

=== "macOS"

    ```
    brew install libsodium
    ```

### Install `pipx`

Check [`pipx` documentation](https://pipx.pypa.io/stable/) for an extended installation tutorial.

=== "Debian"

    ```
    sudo apt install pipx python3-dev
    ```

=== "Fedora"

    ```
    sudo dnf install pipx python3-devel
    ```

=== "macOS"

    ```
    brew install python3 pipx
    ```

## Install with pipx

[pipx](https://pypa.github.io/pipx/) tool is recommended for Python executables.
It isolates libraries into separated virtual environments.

### Ensure path

Takes care to add Pipx executables in the `PATH` of your shell:

```
pipx ensurepath
```

### Install

```
pipx install silkaj
```

### Upgrade

```
pipx upgrade silkaj
```

### Uninstall

```
pipx uninstall silkaj
```

### Check Silkaj is working

```
silkaj
```

______________________________________________________________________

## Install with pip

`pipx` installation is recommended, but you can proceed with `pip` if you know what you are doing.

### Install `pip`

=== "Debian"

    ```
    sudo apt install python3-pip
    ```

=== "Fedora"

    ```
    sudo dnf install python3-pip
    ```

=== "macOS"

    Already installed with `python3`

### Completing `PATH`

If after Silkaj intallation you get a `bash: silkaj: command not found` error, you have to extend the `PATH` with `~./local/bin`:

```bash
echo "export PATH=$PATH:$HOME/.local/bin" >> $HOME/.bashrc
source $HOME/.bashrc
```

### Install for current user only

```
pip install silkaj --user
```

### Upgrade

```
pip install silkaj --user --upgrade
```

### Uninstall (useful to see the real paths)

```
pip uninstall silkaj --user
```

### Check Silkaj is working

```
silkaj
```

## Shell completion

Set up the shell completion by adding following line in your shell configuration depending on your shell:

=== "Bash"

    ```title="$HOME/.bashrc"
    eval "$(_SILKAJ_COMPLETE=bash_source silkaj)"
    ```

=== "Zsh"

    ```title="$HOME/.zshrc"
    eval "$(_SILKAJ_COMPLETE=zsh_source silkaj)"
    ```

=== "Fish"

    ```title="$HOME/.config/fish/completions/silkaj.fish"
    _SILKAJ_COMPLETE=fish_source silkaj | source
    ```

For further details checkout [Click documentation](https://click.palletsprojects.com/en/8.1.x/shell-completion/).

## Install to contribute

Check out the [installation guide with Poetry](./contributing/install_poetry.md).
