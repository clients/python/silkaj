# DeathReaper

<center>
![DeathReaper logo](../images/deathreaper_logo.svg)
</center>

DeathReaper is a service reporting Web of Trust exclusions on [Discourse forums](https://www.discourse.org/).
Images are created containing Silkaj + DeathReaper.
These images are operated on [DeathReaper repository](https://git.duniter.org/clients/python/deathreaper) with pipeline schedules.

### Installation

When installing Silkaj, you have to specify the optional [extra](https://peps.python.org/pep-0508/#extras) `deathreaper` distribution in order to get DeathReaper’s dependency [`pydiscourse`](https://pypi.org/project/pydiscourse/) installed.
It will therefore "activate" the feature, and `silkaj wot exclusions` command will become available.

```sh
pipx install silkaj[deathreaper]
```

With Poetry development environment:

```sh
poetry install --extras deathreaper
```

### Usage

DeathReaper was first released in Silkaj codebase with version 0.12.0.
Make sure `exclusions` command is present under `silkaj wot`, otherwise something probably went wrong with the installation.
Then run `silkaj wot exclusions --help` to check how to use it.

By default it will report the exclusions for the last day, from 24 hours in the past till now.
You can specify an other duration with `silkaj wot exclusions 0.5` for the last 12 hours, half a day, for example.

By default the report will be displayed in the terminal.
To have the report published on a Discourse forum, you have to pass following options `--api-id` `--*-api-key`, `--publish`.
Further code changes have to be done in order to support additional Discourse forums.
