# Multi-recipients transfers and automation

We want to transfer money to multiple recipients and to automate that process.

### Create a recipients file

Create a file (i.e.: `recipients.txt`) containing the list of the recipients public keys you want to send money to.

```title="recipients.txt"
<ABSOLUTE|RELATIVE>

# comment 1
<amount1> <pubkey1>

# comment 2
<amount2> <pubkey2>:[<checksum2>]
```

The file content should be prefixed with `RELATIVE` or `ABSOLUTE` so the amounts will be expressed in UD Ğ1 or Ğ1.

### Set up the authentication

- Check [authentication documentation](./account_storage.md#authentication) on how to handle the authentication.

### Transfer

Finally, you just have to run following command:

```
silkaj -a <account_name> money transfer --file recipients.txt
```

### Automation

In case you want to automate a transfer on a regural basis, on the first day of the month in this example, you can set a `crontab` on your machine (preferably a machine running 7/24):

```
0 0 1 * * silkaj -a <account_name> money transfer --file recipients.txt --yes
```

Pass `--yes` option so no confirmation gets prompted.
In case a password is set on the authentication, pass as well the `--password <password>` general option.
