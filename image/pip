# ------------------------------------------------------------------------------
# Build Stage
# ------------------------------------------------------------------------------
ARG PYTHON_VERS
FROM python:${PYTHON_VERS}-slim AS build

# Install Poetry
RUN pip install poetry

WORKDIR /silkaj

# Copy pyproject.toml and install dependencies
# So this dependencies installation layer
# does not change when the sources changes
# https://python-poetry.org/docs/faq/#poetry-busts-my-docker-cache-because-it-requires-me-to-copy-my-source-files-in-before-installing-3rd-party-dependencies
COPY pyproject.toml .
RUN poetry install --only main --no-root

# Copy source tree, install and build Silkaj
COPY ./ ./
RUN poetry install --only main
RUN poetry build

# ------------------------------------------------------------------------------
# Final Stage
# ------------------------------------------------------------------------------
FROM python:${PYTHON_VERS}-slim

# Set timezone to Paris for DeathReaper
ENV TZ=Europe/Paris

# Create silkaj group and user
RUN groupadd -g 1111 silkaj && \
    useradd -d /silkaj -g silkaj -u 1111 silkaj

# Install libsodium
RUN apt update && \
    apt install --yes libsodium23 && \
    rm -rf /var/lib/apt/lists

# Copy the build artifact from the build stage
COPY --from=build "/silkaj/dist/silkaj-*-py3-none-any.whl" .
RUN pip install silkaj[deathreaper] silkaj-*-py3-none-any.whl

# Use silkaj user
USER silkaj
WORKDIR /usr/local/lib/python${PYTHON_VERS}/site-packages/silkaj

CMD ["/usr/local/bin/silkaj"]
