# Copyright  2016-2025 Maël Azimi <m.a@moul.re>
#
# Silkaj is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Silkaj is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Silkaj. If not, see <https://www.gnu.org/licenses/>.

import re
import sys
from pathlib import Path

import rich_click as click
from duniterpy.key.scrypt_params import ScryptParams
from duniterpy.key.signing_key import SigningKey, SigningKeyException

from silkaj.constants import FAILURE_EXIT_STATUS, PUBKEY_PATTERN
from silkaj.public_key import gen_pubkey_checksum

SEED_HEX_PATTERN = "^[0-9a-fA-F]{64}$"
PUBSEC_PUBKEY_PATTERN = f"pub: ({PUBKEY_PATTERN})"
PUBSEC_SIGNKEY_PATTERN = "sec: ([1-9A-HJ-NP-Za-km-z]{87,90})"


@click.pass_context
def auth_method(ctx: click.Context) -> SigningKey:
    if ctx.obj.get("AUTH_SEED"):
        return auth_by_seed()
    if ctx.obj.get("AUTH_FILE_PATH"):
        return auth_by_auth_file()
    if ctx.obj.get("AUTH_WIF"):
        return auth_by_wif()
    return auth_by_scrypt()


@click.pass_context
def has_auth_method(ctx: click.Context) -> bool:
    return (
        ("AUTH_SCRYPT" in ctx.obj and ctx.obj["AUTH_SCRYPT"])
        or ("AUTH_FILE_PATH" in ctx.obj and ctx.obj["AUTH_FILE_PATH"])
        or ("AUTH_SEED" in ctx.obj and ctx.obj["AUTH_SEED"])
        or ("AUTH_WIF" in ctx.obj and ctx.obj["AUTH_WIF"])
    )


@click.command("authentication", help="Generate authentication file")
@click.argument(
    "auth_file",
    type=click.Path(dir_okay=False, writable=True, path_type=Path),
)
def generate_auth_file(auth_file: Path) -> None:
    key = auth_method()
    pubkey_cksum = gen_pubkey_checksum(key.pubkey)
    if auth_file.is_file():
        message = (
            f"Would you like to erase {auth_file} with an authentication file corresponding \n\
to following pubkey `{pubkey_cksum}`?"
        )
        click.confirm(message, abort=True)
    key.save_seedhex_file(auth_file)
    print(
        f"Authentication file '{auth_file}' generated and stored for public key: {pubkey_cksum}",
    )


@click.pass_context
def auth_by_auth_file(ctx: click.Context) -> SigningKey:
    """
    Uses an authentication file to generate the key
    Authfile can either be:
    * A seed in hexadecimal encoding
    * PubSec format with public and private key in base58 encoding
    """
    authfile = ctx.obj["AUTH_FILE_PATH"]
    filetxt = authfile.read_text(encoding="utf-8")

    # two regural expressions for the PubSec format
    regex_pubkey = re.compile(PUBSEC_PUBKEY_PATTERN, re.MULTILINE)
    regex_signkey = re.compile(PUBSEC_SIGNKEY_PATTERN, re.MULTILINE)

    # Seed hexadecimal format
    if re.search(re.compile(SEED_HEX_PATTERN), filetxt):
        return SigningKey.from_seedhex_file(authfile)
    # PubSec format
    if re.search(regex_pubkey, filetxt) and re.search(regex_signkey, filetxt):
        return SigningKey.from_pubsec_file(authfile)
    sys.exit("Error: the format of the file is invalid")


def auth_by_seed() -> SigningKey:
    seedhex = click.prompt("Please enter your seed on hex format", hide_input=True)
    try:
        return SigningKey.from_seedhex(seedhex)
    # To be fixed upstream in DuniterPy
    except SigningKeyException as error:
        print(error)
        sys.exit(FAILURE_EXIT_STATUS)


@click.pass_context
def auth_by_scrypt(ctx: click.Context) -> SigningKey:
    salt = click.prompt(
        "Please enter your Scrypt Salt (Secret identifier)",
        hide_input=True,
        default="",
    )
    password = click.prompt(
        "Please enter your Scrypt password (masked)",
        hide_input=True,
        default="",
    )

    if ctx.obj["AUTH_SCRYPT_PARAMS"]:
        n, r, p = ctx.obj["AUTH_SCRYPT_PARAMS"].split(",")

        if n.isnumeric() and r.isnumeric() and p.isnumeric():
            n, r, p = int(n), int(r), int(p)
            if n <= 0 or n > 65536 or r <= 0 or r > 512 or p <= 0 or p > 32:
                sys.exit("Error: the values of Scrypt parameters are not good")
            scrypt_params = ScryptParams(n, r, p)
        else:
            sys.exit("one of n, r or p is not a number")
    else:
        scrypt_params = None

    try:
        return SigningKey.from_credentials(salt, password, scrypt_params)
    except ValueError as error:
        print(error)
        sys.exit(FAILURE_EXIT_STATUS)


def auth_by_wif() -> SigningKey:
    wif_hex = click.prompt(
        "Enter your WIF or Encrypted WIF address (masked)",
        hide_input=True,
    )
    password = click.prompt(
        "(Leave empty in case WIF format) Enter the Encrypted WIF password (masked)",
        hide_input=True,
    )
    try:
        return SigningKey.from_wif_or_ewif_hex(wif_hex, password)
    # To be fixed upstream in DuniterPy
    except SigningKeyException as error:
        print(error)
        sys.exit(FAILURE_EXIT_STATUS)
