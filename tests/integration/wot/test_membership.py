# Copyright  2016-2025 Maël Azimi <m.a@moul.re>
#
# Silkaj is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Silkaj is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Silkaj. If not, see <https://www.gnu.org/licenses/>.

from unittest.mock import Mock

import pytest
from click.testing import CliRunner
from duniterpy.documents import get_block_id
from duniterpy.key import SigningKey

from silkaj import auth
from silkaj.blockchain import tools as bc_tools
from silkaj.cli import cli
from silkaj.wot import membership
from silkaj.wot import tools as w_tools
from tests.patched.blockchain_tools import fake_block_id, patched_get_head_block

# Values and patches
PUBKEY = "EA7Dsw39ShZg4SpURsrgMaMqrweJPUFPYHwZA8e92e3D"
identity_block_id = get_block_id(
    "0-E3B0C44298FC1C149AFBF4C8996FB92427AE41E4649B934CA495991B7852B855",
)
identity_uid = "toto"

membership_block_id = fake_block_id


def patched_auth_method():
    return SigningKey.from_credentials(identity_uid, identity_uid)


def patched_choose_identity(pubkey):
    return (
        {"uid": identity_uid, "meta": {"timestamp": identity_block_id}},
        PUBKEY,
        None,
    )


@pytest.mark.parametrize(
    ("dry_run", "display", "confirmation"),
    [
        (True, False, False),
        (False, True, False),
        (False, True, True),
        (False, False, True),
    ],
)
def test_membership_cmd(dry_run, display, confirmation, monkeypatch):
    # Monkeypatch and Mock
    monkeypatch.setattr(auth, "auth_method", patched_auth_method)
    monkeypatch.setattr(bc_tools, "get_head_block", patched_get_head_block)
    monkeypatch.setattr(w_tools, "choose_identity", patched_choose_identity)

    patched_display_confirmation_table = Mock()
    monkeypatch.setattr(
        membership,
        "display_confirmation_table",
        patched_display_confirmation_table,
    )
    if not dry_run and not display:
        patched_generate_membership_document = Mock()
        monkeypatch.setattr(
            membership,
            "generate_membership_document",
            patched_generate_membership_document,
        )

    # Run membership command
    command = []
    if dry_run:
        command += ["--dry-run"]
    if display:
        command += ["--display"]
    command += ["wot", "membership"]
    pass_license = "No\nYes\n"
    confirmations = pass_license + ("Yes" if confirmation else "No")
    result = CliRunner().invoke(cli, args=command, input=confirmations)

    # Assert functions are called
    patched_display_confirmation_table.assert_called_once_with(
        identity_uid,
        PUBKEY,
        identity_block_id,
    )
    if dry_run or display:
        assert "Type: Membership" in result.output
    else:
        #    signing_key = patched_auth_method()
        patched_generate_membership_document.assert_called_once()
    #   membership_block_id is different
    #   patched_generate_membership_document.assert_called_once_with(
    #       PUBKEY,
    #       membership_block_id,
    #       identity_uid,
    #       identity_block_id,
    #       currency,
    #       signing_key,
    #   )
