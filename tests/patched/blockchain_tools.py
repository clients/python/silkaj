# Copyright  2016-2025 Maël Azimi <m.a@moul.re>
#
# Silkaj is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Silkaj is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Silkaj. If not, see <https://www.gnu.org/licenses/>.

# This file contains fake values for testing purposes

from duniterpy.documents.block_id import BlockID

currency = "g1"

mocked_block = {
    "number": 48000,
    "time": 1592243760,
    "unitbase": 0,
    "currency": currency,
    "hash": "0000010D30B1284D34123E036B7BE0A449AE9F2B928A77D7D20E3BDEAC7EE14C",
}

mocked_block_gtest = {
    "number": 48000,
    "time": 1592243760,
    "unitbase": 0,
    "currency": "g1-test",
    "hash": "0000010D30B1284D34123E036B7BE0A449AE9F2B928A77D7D20E3BDEAC7EE14C",
}

fake_block_id = BlockID(
    48000,
    "0000010D30B1284D34123E036B7BE0A449AE9F2B928A77D7D20E3BDEAC7EE14C",
)


def patched_params(self):
    return {
        "msValidity": 31557600,
        "msPeriod": 5259600,
    }


def patched_block(self, number):
    return mocked_block


# mock get_head_block()
def patched_get_head_block():
    return mocked_block


def patched_get_head_block_gtest():
    return mocked_block_gtest
