# Copyright  2016-2025 Maël Azimi <m.a@moul.re>
#
# Silkaj is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Silkaj is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Silkaj. If not, see <https://www.gnu.org/licenses/>.

from duniterpy.documents.block_id import BlockID
from duniterpy.documents.identity import Identity

idty1 = Identity(
    currency="g1-test",
    pubkey="6upqFiJ66WV6N3bPc8x8y7rXT3syqKRmwnVyunCtEj7o",
    uid="Claude",
    block_id=BlockID.from_str(
        "597334-002A45E751DCA7535D4F0A082F493E2C8EFF07612683525EB5DA92B6D17C30BD",
    ),
)
idty1.signature = "kFW2we2K3zx4PZODx0Wf+xdXAJTmYD+yqdyZBsPF7UwqdaCA4N\
+yHj7+09Gjsttl0i9GtWzodyJ6mBE1q7jcAw=="

lookup_one = {
    "partial": False,
    "results": [
        {
            "pubkey": "6upqFiJ66WV6N3bPc8x8y7rXT3syqKRmwnVyunCtEj7o",
            "uids": [
                {
                    "uid": "Claude",
                    "meta": {
                        "timestamp": "597334-002A45E751DCA7535D4F0A08\
2F493E2C8EFF07612683525EB5DA92B6D17C30BD",
                    },
                    "revoked": False,
                    "revoked_on": None,
                    "revocation_sig": None,
                    "self": "kFW2we2K3zx4PZODx0Wf+xdXAJTmYD+yqdyZBsPF\
7UwqdaCA4N+yHj7+09Gjsttl0i9GtWzodyJ6mBE1q7jcAw==",
                    "others": [],
                },
            ],
        },
    ],
}


lookup_two = {
    "partial": False,
    "results": [
        {
            "pubkey": "6upqFiJ66WV6N3bPc8x8y7rXT3syqKRmwnVyunCtEj7o",
            "uids": [
                {
                    "uid": "Claude",
                    "meta": {
                        "timestamp": "597334-002A45E751DCA7535D4F0A08\
2F493E2C8EFF07612683525EB5DA92B6D17C30BD",
                    },
                    "revoked": False,
                    "revoked_on": None,
                    "revocation_sig": None,
                    "self": "kFW2we2K3zx4PZODx0Wf+xdXAJTmYD+yqdyZBsPF\
7UwqdaCA4N+yHj7+09Gjsttl0i9GtWzodyJ6mBE1q7jcAw==",
                    "others": [],
                },
                {
                    "uid": "Claudia",
                    "meta": {
                        "timestamp": "597334-002A45E751DCA7535D4F0A08\
2F493E2C8EFF07612683525EB5DA92B6D17C30BD",
                    },
                    "revoked": False,
                    "revoked_on": None,
                    "revocation_sig": None,
                    "self": "kFW2we2K3zx4PZODx0Wf+xdXAJTmYD+yqdyZBsPF\
7UwqdaCA4N+yHj7+09Gjsttl0i9GtWzodyJ6mBE1q7jcAw==",
                    "others": [],
                },
            ],
        },
    ],
}


lookup_three = {
    "partial": False,
    "results": [
        {
            "pubkey": "6upqFiJ66WV6N3bPc8x8y7rXT3syqKRmwnVyunCtEj7o",
            "uids": [
                {
                    "uid": "Claude",
                    "meta": {
                        "timestamp": "597334-002A45E751DCA7535D4F0A08\
2F493E2C8EFF07612683525EB5DA92B6D17C30BD",
                    },
                    "revoked": False,
                    "revoked_on": None,
                    "revocation_sig": None,
                    "self": "kFW2we2K3zx4PZODx0Wf+xdXAJTmYD+yqdyZBsPF\
7UwqdaCA4N+yHj7+09Gjsttl0i9GtWzodyJ6mBE1q7jcAw==",
                    "others": [],
                },
            ],
        },
        {
            "pubkey": "XXXXXXJ66WV6N3bPc8x8y7rXT3syqKRmwnVyunCtEj7o",
            "uids": [
                {
                    "uid": "Claude",
                    "meta": {
                        "timestamp": "597334-002A45E751DCA7535D4F0A08\
2F493E2C8EFF07612683525EB5DA92B6D17C30BD",
                    },
                    "revoked": False,
                    "revoked_on": None,
                    "revocation_sig": None,
                    "self": "kFW2we2K3zx4PZODx0Wf+xdXAJTmYD+yqdyZBsPF\
7UwqdaCA4N+yHj7+09Gjsttl0i9GtWzodyJ6mBE1q7jcAw==",
                    "others": [],
                },
            ],
        },
    ],
}


idty2 = Identity(
    currency="g1-test",
    pubkey="969qRJs8KhsnkyzqarpL4RKZGMdVKNbZgu8fhsigM7Lj",
    uid="aa_aa",
    block_id=BlockID.from_str(
        "703902-00002D6BC5E4FC540A4E188C3880A0ACCA06CD77017D26231A515312162B4070",
    ),
)
idty2.signature = "3RNQcKNI1VMmuCpK7wer8haOA959EQSDIR1v0Ue/7TpTCOmsU2\
zYCpC+tqgLQFxDX4A79sB61c11J5C/3Z/TCw=="


idty_block = {
    "version": 12,
    "nonce": 10100000000525,
    "number": 597334,
    "powMin": 45,
    "time": 1594980185,
    "medianTime": 1594978717,
    "membersCount": 7,
    "monetaryMass": 2157680864,
    "unitbase": 3,
    "issuersCount": 3,
    "issuersFrame": 16,
    "issuersFrameVar": 0,
    "currency": "g1-test",
    "issuer": "3dnbnYY9i2bHMQUGyFp5GVvJ2wBkVpus31cDJA5cfRpj",
    "signature": "Eme9mi25DtUQrP3Hk6evJBQP6GRU0asJrl9G2RWUgtB71AMOWqs\
/NeraG8YBwQEGokQg1mHMUv7fEoUiEetwDw==",
    "hash": "002A45E751DCA7535D4F0A082F493E2C8EFF07612683525EB5DA92B6D17C30BD",
    "parameters": "",
    "previousHash": "0023B87885C52CDE75694C71BED1237B5C7B686C00AB68C8D75693513E1F8765",
    "previousIssuer": "39YyHCMQNmXY7NkPCXXfzpV1vYct4GBxwgfyd4d72HmB",
    "inner_hash": "46D99F8431053892F230E4E07EC16A2A68B09D68EBC3F9FD796289493AFAFFB5",
    "dividend": None,
    "identities": [],
    "joiners": [],
    "actives": [],
    "leavers": [],
    "revoked": [],
    "excluded": [],
    "certifications": [],
    "transactions": [],
    "raw": "Version: 12\nType: Block\nCurrency: g1-test\nNumber: 597334\n\
PoWMin: 45\nTime: 1594980185\nMedianTime: 1594978717\nUnitBase: 3\n\
Issuer: 3dnbnYY9i2bHMQUGyFp5GVvJ2wBkVpus31cDJA5cfRpj\nIssuersFrame: 16\n\
IssuersFrameVar: 0\nDifferentIssuersCount: 3\n\
PreviousHash: 0023B87885C52CDE75694C71BED1237B5C7B686C00AB68C8D75693513E1F8765\n\
PreviousIssuer: 39YyHCMQNmXY7NkPCXXfzpV1vYct4GBxwgfyd4d72HmB\nMembersCount: 7\n\
Identities:\nJoiners:\nActives:\nLeavers:\nRevoked:\nExcluded:\nCertifications:\n\
Transactions:\nInnerHash: 46D99F8431053892F230E4E07EC16A2A68B09D68EBC3F9FD796289493AFAFFB5\n\
Nonce: 10100000000525\n",
}
