# Copyright  2016-2025 Maël Azimi <m.a@moul.re>
#
# Silkaj is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Silkaj is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Silkaj. If not, see <https://www.gnu.org/licenses/>.

import csv
from pathlib import Path

import arrow
import pytest
from click.testing import CliRunner

from silkaj import tools
from silkaj.cli import cli
from silkaj.constants import (
    ALL_DIGITAL,
    PUBKEY_MAX_LENGTH,
    PUBKEY_MIN_LENGTH,
    SHORT_PUBKEY_SIZE,
)
from silkaj.money import history
from silkaj.money import tools as m_tools
from silkaj.public_key import CHECKSUM_SIZE
from silkaj.wot import tools as wt
from tests.patched.blockchain_tools import currency
from tests.patched.money import patched_get_ud_value
from tests.patched.tools import patched_get_currency_symbol
from tests.patched.tx_history import patched_get_transactions_history
from tests.patched.wot import patched_identities_from_pubkeys

SHORT_PUBKEY_LENGTH_WITH_CHECKSUM = (
    SHORT_PUBKEY_SIZE + CHECKSUM_SIZE + 2
)  # 2 chars "…:" ==> 8 + 3 + 2 = 13

MIN_FULL_PUBKEY_LENGTH_WITH_CHECKSUM = (
    PUBKEY_MIN_LENGTH + CHECKSUM_SIZE + 1
)  # char `:` ==> 43 + 3 + 1 = 47

MAX_FULL_PUBKEY_LENGTH_WITH_CHECKSUM = (
    PUBKEY_MAX_LENGTH + CHECKSUM_SIZE + 1
)  # char `:` ==> 44 + 3 + 1 = 48


def test_history_generate_txs_list_and_pubkey_uid_display(monkeypatch):
    def min_pubkey_length_with_uid(pubkey):
        # uid is at least one char : "<uid> - <pubkey>" adds min 4 chars to <pubkey>
        return pubkey + 4

    monkeypatch.setattr(wt, "identities_from_pubkeys", patched_identities_from_pubkeys)

    client = "whatever"
    ud_value = 10.07
    table_columns = 5
    pubkey = "d88fPFbDdJXJANHH7hedFMaRyGcnVZj9c5cDaE76LRN"

    received_txs, sent_txs = [], []
    patched_get_transactions_history(client, pubkey, received_txs, sent_txs)

    # simple table
    txs_list = history.generate_txs_list(
        received_txs,
        sent_txs,
        pubkey,
        ud_value,
        currency,
        uids=False,
        full_pubkey=False,
    )
    for tx_list in txs_list:
        assert len(tx_list) == table_columns
        assert "…:" in tx_list[1]
        assert len(tx_list[1]) == SHORT_PUBKEY_LENGTH_WITH_CHECKSUM

    # with uids
    txs_list_uids = history.generate_txs_list(
        received_txs,
        sent_txs,
        pubkey,
        ud_value,
        currency,
        uids=True,
        full_pubkey=False,
    )
    for tx_list in txs_list_uids:
        assert len(tx_list) == table_columns
        assert "…:" in tx_list[1]
    # check all lines
    assert len(txs_list_uids[0][1]) >= min_pubkey_length_with_uid(
        SHORT_PUBKEY_LENGTH_WITH_CHECKSUM,
    )
    assert len(txs_list_uids[1][1]) == SHORT_PUBKEY_LENGTH_WITH_CHECKSUM
    assert len(txs_list_uids[2][1]) >= min_pubkey_length_with_uid(
        SHORT_PUBKEY_LENGTH_WITH_CHECKSUM,
    )
    assert len(txs_list_uids[3][1]) == SHORT_PUBKEY_LENGTH_WITH_CHECKSUM

    # with full pubkeys
    txs_list_full = history.generate_txs_list(
        received_txs,
        sent_txs,
        pubkey,
        ud_value,
        currency,
        uids=False,
        full_pubkey=True,
    )
    for tx_list in txs_list_full:
        assert len(tx_list) == table_columns
        assert "…:" not in tx_list[1]
        assert ":" in tx_list[1]
        # this length is not true for multisig txs, which are very unlikely for now.
        assert (
            len(tx_list[1]) == MIN_FULL_PUBKEY_LENGTH_WITH_CHECKSUM
            or len(tx_list[1]) == MAX_FULL_PUBKEY_LENGTH_WITH_CHECKSUM
        )

    # with full pubkeys and uids
    txs_list_uids_full = history.generate_txs_list(
        received_txs,
        sent_txs,
        pubkey,
        ud_value,
        currency,
        uids=True,
        full_pubkey=True,
    )
    for tx_list in txs_list_uids_full:
        assert len(tx_list) == table_columns
        assert "…:" not in tx_list[1]
        assert ":" in tx_list[1]
    # check all lines
    assert len(txs_list_uids_full[0][1]) >= min_pubkey_length_with_uid(
        MIN_FULL_PUBKEY_LENGTH_WITH_CHECKSUM,
    )
    assert (
        len(txs_list_uids_full[1][1]) == MIN_FULL_PUBKEY_LENGTH_WITH_CHECKSUM
        or len(txs_list_uids_full[1][1]) == MAX_FULL_PUBKEY_LENGTH_WITH_CHECKSUM
    )
    assert len(txs_list_uids_full[2][1]) >= min_pubkey_length_with_uid(
        MIN_FULL_PUBKEY_LENGTH_WITH_CHECKSUM,
    )
    assert (
        len(txs_list_uids_full[3][1]) == MIN_FULL_PUBKEY_LENGTH_WITH_CHECKSUM
        or len(txs_list_uids_full[3][1]) == MAX_FULL_PUBKEY_LENGTH_WITH_CHECKSUM
    )


@pytest.mark.parametrize(
    ("tx_addresses", "outputs", "occurence", "return_value"),
    [
        (None, None, 0, ""),
        (None, None, 1, "\n"),
        (None, ["output1"], 0, ""),
        (None, ["output1"], 1, ""),
        (None, ["output1", "output2"], 0, "\n"),
        (None, ["output1", "output2"], 1, "\n"),
        ("pubkey", None, 0, ""),
        ("pubkey", None, 1, "\n"),
        ("pubkey", ["output1"], 0, ""),
        ("pubkey", ["output1"], 1, ""),
        ("Total", ["output1", "output2"], 0, "\n"),
        ("pubkey", ["output1", "output2"], 0, "\n"),
        ("pubkey", ["output1", "output2"], 1, "\n"),
    ],
)
def test_prefix(tx_addresses, outputs, occurence, return_value):
    assert history.prefix(tx_addresses, outputs, occurence) == return_value


csv_reference = (
    ["Date", "Issuers/Recipients", "Amounts Ğ1", "Amounts UDĞ1", "Comment"],
    [
        arrow.get(111111114).to("local").format(ALL_DIGITAL),
        "CvrMiUhAJpNyX5sdAyZqPE6yEFfSsf6j9EpMmeKvMCWW:DNB",
        "1.0",
        "0.09041591320072333",
        "initialisation",
    ],
    [
        arrow.get(111111113).to("local").format(ALL_DIGITAL),
        "CmFKubyqbmJWbhyH2eEPVSSs4H4NeXGDfrETzEnRFtPd:CQ5",
        "1.0",
        "0.09041591320072333",
        "",
    ],
    [
        arrow.get(111111112).to("local").format(ALL_DIGITAL),
        "CvrMiUhAJpNyX5sdAyZqPE6yEFfSsf6j9EpMmeKvMCWW:DNB",
        "-1.0",
        "-0.09",
        "",
    ],
    [
        arrow.get(111111111).to("local").format(ALL_DIGITAL),
        "CmFKubyqbmJWbhyH2eEPVSSs4H4NeXGDfrETzEnRFtPd:CQ5",
        "-1.0",
        "-0.09",
        "",
    ],
)


def test_csv_output(monkeypatch):
    monkeypatch.setattr(m_tools, "get_ud_value", patched_get_ud_value)
    monkeypatch.setattr(tools, "get_currency_symbol", patched_get_currency_symbol)
    monkeypatch.setattr(
        history,
        "get_transactions_history",
        patched_get_transactions_history,
    )

    file = "history.csv"
    pubkey = "d88fPFbDdJXJANHH7hedFMaRyGcnVZj9c5cDaE76LRN"
    runner = CliRunner()
    with runner.isolated_filesystem():
        result = runner.invoke(cli, ["money", "history", pubkey, "--csv-file", file])
        assert result.output == f"{file} file successfully saved!\n"
        assert result.exit_code == 0

        with Path(file).open() as f:
            reader = csv.reader(f)
            for row_file, row_ref in zip(reader, csv_reference):
                assert row_file == row_ref
