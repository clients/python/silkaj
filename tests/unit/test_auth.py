# Copyright  2016-2025 Maël Azimi <m.a@moul.re>
#
# Silkaj is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Silkaj is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Silkaj. If not, see <https://www.gnu.org/licenses/>.

import pytest

from silkaj import auth
from tests import helpers
from tests.patched.auth import (
    patched_auth_by_auth_file,
    patched_auth_by_scrypt,
    patched_auth_by_seed,
    patched_auth_by_wif,
)


@pytest.mark.parametrize(
    ("context", "auth_method_called"),
    [
        ({"auth_seed": True}, "call_auth_by_seed"),
        ({"auth_file_path": True}, "call_auth_by_auth_file"),
        ({"auth_wif": True}, "call_auth_by_wif"),
        ({}, "call_auth_by_scrypt"),
    ],
)
def test_auth_method(context, auth_method_called, monkeypatch):
    monkeypatch.setattr(auth, "auth_by_seed", patched_auth_by_seed)
    monkeypatch.setattr(auth, "auth_by_wif", patched_auth_by_wif)
    monkeypatch.setattr(auth, "auth_by_auth_file", patched_auth_by_auth_file)
    monkeypatch.setattr(auth, "auth_by_scrypt", patched_auth_by_scrypt)
    helpers.define_click_context(**context)
    assert auth_method_called == auth.auth_method()
