# Copyright  2016-2025 Maël Azimi <m.a@moul.re>
#
# Silkaj is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Silkaj is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Silkaj. If not, see <https://www.gnu.org/licenses/>.

import shutil

import pytest
from texttable import Texttable

from silkaj import tui


def test_create_table():
    expected = Texttable(max_width=shutil.get_terminal_size().columns)
    expected.add_rows([["one", "two"], ["three", "four"]])
    expected.set_chars(tui.VERT_TABLE_CHARS)

    test_table = tui.Table().add_rows([["one", "two"], ["three", "four"]])
    assert expected.draw() == test_table.draw()

    expected.set_deco(expected.HEADER | expected.VLINES | expected.BORDER)
    test_table = tui.Table("columns").add_rows([["one", "two"], ["three", "four"]])
    assert expected.draw() == test_table.draw()


@pytest.mark.parametrize(
    ("rows", "header", "expected"),
    [
        (
            [
                ["three", "o'clock"],
                ["four", "o'clock"],
                ["rock", "rock around the clock"],
            ],
            ["one", "two"],
            "│───────│───────────────────────│\n\
│  one  │          two          │\n\
│═══════│═══════════════════════│\n\
│ three │ o'clock               │\n\
│───────│───────────────────────│\n\
│ four  │ o'clock               │\n\
│───────│───────────────────────│\n\
│ rock  │ rock around the clock │\n\
│───────│───────────────────────│",
        ),
        (
            [
                ["three", "o'clock"],
                ["four", "o'clock"],
                ["rock", "rock around the clock"],
            ],
            None,
            "│───────│───────────────────────│\n\
│ three │ o'clock               │\n\
│───────│───────────────────────│\n\
│ four  │ o'clock               │\n\
│───────│───────────────────────│\n\
│ rock  │ rock around the clock │\n\
│───────│───────────────────────│",
        ),
        (
            [
                ["three", "o'clock"],
                ["four", "o'clock"],
                ["rock"],
            ],
            ["one", "two"],
            False,
        ),
    ],
)
def test_fill_rows(rows, header, expected):
    table = tui.Table()
    if not expected:
        with pytest.raises(AssertionError):
            table.fill_rows(rows, header)
    else:
        table.fill_rows(rows, header)
        assert table.draw() == expected


@pytest.mark.parametrize(
    ("dict_", "expected"),
    [
        (
            {
                "one": ["three", "four", "rock"],
                "two": ["o'clock", "o'clock", "rock around the clock"],
            },
            "│───────│───────────────────────│\n\
│  one  │          two          │\n\
│═══════│═══════════════════════│\n\
│ three │ o'clock               │\n\
│───────│───────────────────────│\n\
│ four  │ o'clock               │\n\
│───────│───────────────────────│\n\
│ rock  │ rock around the clock │\n\
│───────│───────────────────────│",
        ),
    ],
)
def test_fill_from_dict(dict_, expected):
    table = tui.Table()
    table.fill_from_dict(dict_)
    assert table.draw() == expected


@pytest.mark.parametrize(
    ("dict_list", "expected"),
    [
        (
            (
                [
                    {"one": "three", "two": "o'clock"},
                    {"one": "four", "two": "o'clock"},
                    {"one": "rock", "two": "rock around the clock"},
                ]
            ),
            "│───────│───────────────────────│\n\
│  one  │          two          │\n\
│═══════│═══════════════════════│\n\
│ three │ o'clock               │\n\
│───────│───────────────────────│\n\
│ four  │ o'clock               │\n\
│───────│───────────────────────│\n\
│ rock  │ rock around the clock │\n\
│───────│───────────────────────│",
        ),
    ],
)
def test_fill_from_dict_list(dict_list, expected):
    table = tui.Table()
    table.fill_from_dict_list(dict_list)
    assert table.draw() == expected
