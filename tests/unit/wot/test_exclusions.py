# Copyright  2016-2025 Maël Azimi <m.a@moul.re>
#
# Silkaj is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Silkaj is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Silkaj. If not, see <https://www.gnu.org/licenses/>.

import pytest

pytest.importorskip(
    "pydiscourse",
    reason="pydiscourse can't be imported, skipping deathreaper tests",
)

from silkaj.wot import exclusions


@pytest.mark.parametrize(
    (
        "api_id",
        "duniter_forum_api_key",
        "ml_forum_api_key",
        "publish",
        "currency",
        "error",
    ),
    [
        (None, None, None, False, "g1", False),
        (None, None, None, True, "g1", True),
        ("DeathReaper", None, None, True, "g1", True),
        (None, "key", None, True, "g1", True),
        (None, None, "key", True, "g1", True),
        ("DeathReaper", "key", "key", True, "g1", False),
        ("DeathReaper", "key", None, True, "g1-test", False),
    ],
)
def test_check_options(
    api_id,
    duniter_forum_api_key,
    ml_forum_api_key,
    publish,
    currency,
    error,
    capsys,
):
    if error:
        msg = "Error: To be able to publish, api_id"
        with pytest.raises(SystemExit, match=msg):
            exclusions.check_options(
                api_id,
                duniter_forum_api_key,
                ml_forum_api_key,
                publish,
                currency,
            )
    else:
        exclusions.check_options(
            api_id,
            duniter_forum_api_key,
            ml_forum_api_key,
            publish,
            currency,
        )
        assert not capsys.readouterr().out


# def test_generate_identity_info():
#     ref = "- **Certifié·e par** @GildasMalassinet, @Marcolibri, @Gregory et @gpsqueeek.\n\
# - **A certifié** @lesailesbleues, @Marcolibri, @Gregory et @LaureBLodjtahne.\n\
# - **Exclu·e le** 2020-01-27 04:06:09\n\
# - **Raison de l'exclusion** : manque de certifications"
#     # missing lookup and block
#     # 292110
#     raw_block = "Version: 11\nType: Block\nCurrency: g1\nNumber: 292110\nPoWMin: 89\n\
# Time: 1580098477\nMedianTime: 1580094369\nUnitBase: 0\n\
# Issuer: D3krfq6J9AmfpKnS3gQVYoy7NzGCc61vokteTS8LJ4YH\nIssuersFrame: 161\n\
# IssuersFrameVar: 0\nDifferentIssuersCount: 32\n\
# PreviousHash: 0000009F88010580F54C0403BC87BF10E912D0FBC274294343DDC748F697991D\n\
# PreviousIssuer: 5P9HB1uBdE3ZqThSatBdBbjVmh3psa1BhsCRBumn1aMo\nMembersCount: 2527\n\
# Identities:\nJoiners:\nActives:\nLeavers:\nRevoked:\nExcluded:\n\
# DKWXiEEd4wkf4pgd95vkGnNkHxzTKDpPPSgMay5WJ7Fc\nCertifications:\nTransactions:\n\
# InnerHash: CD17661EBD858CC374918D2F94363E22AB5BDC617FDEEA729C384616FD2285C4\n\
# Nonce: 10800000223569\n"
#     block_sig = "GvREqrzvbS2RHoLCEbfjjPcsJ+G+jVKqejF5fu9i3076XJ4pmc+udyCPoJ5TiCo7OPB5GOLmsCLocJZbqQS1CA=="
#     block = Block.from_signed_raw(raw_block + block_sig + "\n")
#     gen = exclusions.generate_identity_info(lookup, block, 5)
#     assert gen == ref


def test_elements_inbetween_list():
    uids = ["toto", "titi", "tata"]
    assert exclusions.elements_inbetween_list(0, uids) == " "
    assert exclusions.elements_inbetween_list(1, uids) == ", "
    assert exclusions.elements_inbetween_list(2, uids) == " et "


@pytest.mark.parametrize(
    ("message", "publish", "currency", "forum"),
    [("message", False, "g1", "duniter")],
)
def test_publish_display(message, publish, currency, forum, capsys):
    exclusions.publish_display(None, None, message, publish, currency, forum)
    captured = capsys.readouterr()
    if not publish:
        assert captured.out == "message\n"


@pytest.mark.parametrize(
    ("currency", "forum"),
    [("g1", "duniter"), ("g1", "monnaielibre"), ("g1-test", "duniter")],
)
def test_get_topic_id(currency, forum):
    expected_topic_id = exclusions.get_topic_id(currency, forum)
    if currency == "g1":
        if forum == "duniter":
            topic_id = exclusions.DUNITER_FORUM_G1_TOPIC_ID
        else:
            topic_id = exclusions.MONNAIE_LIBRE_FORUM_G1_TOPIC_ID
    else:
        topic_id = exclusions.DUNITER_FORUM_GTEST_TOPIC_ID
    assert topic_id == expected_topic_id


@pytest.mark.parametrize(
    ("forum", "response", "topic_id"),
    [
        ("duniter", {"topic_slug": "topic-slug"}, exclusions.DUNITER_FORUM_G1_TOPIC_ID),
        (
            "monnaielibre",
            {"topic_slug": "silkaj"},
            exclusions.MONNAIE_LIBRE_FORUM_G1_TOPIC_ID,
        ),
        (
            "duniter",
            {"topic_slug": "how-to-monnaie-libre"},
            exclusions.DUNITER_FORUM_G1_TOPIC_ID,
        ),
    ],
)
def test_publication_link(forum, response, topic_id, capsys):
    if forum == "duniter":
        forum_url = exclusions.DUNITER_FORUM_URL
    else:
        forum_url = exclusions.MONNAIE_LIBRE_FORUM_URL
    expected = "Published on {}t/{}/{}/last\n".format(
        forum_url,
        response["topic_slug"],
        str(topic_id),
    )
    exclusions.publication_link(forum, response, topic_id)
    captured = capsys.readouterr()
    assert expected == captured.out
