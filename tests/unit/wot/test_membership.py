# Copyright  2016-2025 Maël Azimi <m.a@moul.re>
#
# Silkaj is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Silkaj is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with Silkaj. If not, see <https://www.gnu.org/licenses/>.

import arrow
import pytest
from duniterpy.api import bma
from duniterpy.documents import Membership, get_block_id
from duniterpy.key import SigningKey

from silkaj import tui
from silkaj.blockchain import tools as bc_tools
from silkaj.constants import DATE
from silkaj.network import client_instance
from silkaj.public_key import gen_pubkey_checksum
from silkaj.wot import membership
from tests.patched.blockchain_tools import (
    currency,
    fake_block_id,
    patched_block,
    patched_params,
)
from tests.patched.wot import (
    patched_wot_requirements_no_pending,
    patched_wot_requirements_one_pending,
)

# Values and patches
PUBKEY = "EA7Dsw39ShZg4SpURsrgMaMqrweJPUFPYHwZA8e92e3D"
identity_block_id = get_block_id(
    "0-E3B0C44298FC1C149AFBF4C8996FB92427AE41E4649B934CA495991B7852B855",
)
identity_uid = "toto"

membership_block_id = fake_block_id


def patched_auth_method():
    return SigningKey.from_credentials(identity_uid, identity_uid)


@pytest.mark.parametrize(
    "patched_wot_requirements",
    [patched_wot_requirements_no_pending, patched_wot_requirements_one_pending],
)
def test_display_confirmation_table(patched_wot_requirements, monkeypatch, capsys):
    monkeypatch.setattr(bma.wot, "requirements", patched_wot_requirements)
    monkeypatch.setattr(bma.blockchain, "parameters", patched_params)
    monkeypatch.setattr(bma.blockchain, "block", patched_block)

    client = client_instance()
    identities_requirements = client(bma.wot.requirements, search=PUBKEY, pubkey=True)
    for identity_requirements in identities_requirements["identities"]:
        if identity_requirements["uid"] == identity_uid:
            membership_expires = identity_requirements["membershipExpiresIn"]
            pending_expires = identity_requirements["membershipPendingExpiresIn"]
            pending_memberships = identity_requirements["pendingMemberships"]
            break

    table = []
    if membership_expires:
        table.append(
            [
                "Expiration date of current membership",
                arrow.now().shift(seconds=membership_expires).humanize(),
            ],
        )

    if pending_memberships:
        table.append(
            [
                "Number of pending membership(s) in the mempool",
                len(pending_memberships),
            ],
        )
        table.append(
            [
                "Pending membership documents will expire",
                arrow.now().shift(seconds=pending_expires).humanize(),
            ],
        )

    table.append(["User Identifier (UID)", identity_uid])
    table.append(["Public Key", gen_pubkey_checksum(PUBKEY)])

    table.append(["Block Identity", str(identity_block_id)[:45] + "…"])

    block = client(bma.blockchain.block, identity_block_id.number)
    table.append(
        [
            "Identity published",
            arrow.get(block["time"]).to("local").format(DATE),
        ],
    )

    params = bc_tools.get_blockchain_parameters()
    table.append(
        [
            "Expiration date of new membership",
            arrow.now().shift(seconds=params["msValidity"]).humanize(),
        ],
    )

    table.append(
        [
            "Expiration date of new membership from the mempool",
            arrow.now().shift(seconds=params["msPeriod"]).humanize(),
        ],
    )

    display_table = tui.Table()
    display_table.fill_rows(table)
    expected = display_table.draw() + "\n"

    membership.display_confirmation_table(identity_uid, PUBKEY, identity_block_id)
    captured = capsys.readouterr()
    assert expected == captured.out


def test_generate_membership_document():
    signing_key = patched_auth_method()
    generated_membership = membership.generate_membership_document(
        PUBKEY,
        membership_block_id,
        identity_uid,
        identity_block_id,
        currency=currency,
        key=signing_key,
    )
    expected = Membership(
        issuer=PUBKEY,
        membership_block_id=membership_block_id,
        uid=identity_uid,
        identity_block_id=identity_block_id,
        signing_key=signing_key,
        currency=currency,
    )
    assert expected == generated_membership
